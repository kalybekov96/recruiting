import {Dialog, DialogContent, DialogContentText, DialogActions, Button} from "@mui/material";

export function ConfirmDialog({open, handleDialogChange, photoId, handleDeletePhoto}) {
  return (
    <Dialog
      open={open}
      onClose={() => handleDialogChange({open: false, agreement: false})}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
            Do you really want to delete this photo?
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => handleDialogChange({open: false, agreement: false})}>Disagree</Button>
        <Button onClick={() => handleDeletePhoto(photoId)} autoFocus>
          Agree
        </Button>
      </DialogActions>
    </Dialog>
  );
}