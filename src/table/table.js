import { useEffect, useState } from "react";
import {
  Chip,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from "@mui/material";
import { BasicModal } from "../modal/Modal";
import { LinearColor } from "../loader/loader";
import { ConfirmDialog } from "../confirm-dialog/confirm-dialog";

export function AppTable({ albumId, limit, page, changeLength }) {
  const [dialog, setDialog] = useState({
    open: false,
    agreement: false,
    id: undefined,
  });
  const [modal, setModal] = useState({ open: false, url: null });
  const [photos, setPhotos] = useState({ loading: false, data: [] });

  useEffect(() => {
    fetchPhotos();
  }, [albumId, page, limit]);

  const deletePhoto = async (id) => {
    try {
      setDialog({ open: false, agreement: false, id: undefined });
      await fetch(`https://jsonplaceholder.typicode.com/photos/${id}`, {
        method: "DELETE",
      });
      fetchPhotos();
    } catch (e) {
      console.error(e);
    }
  };

  async function fetchPhotos() {
    try {
      setPhotos({ loading: true, data: [] });
      const dataLength = await fetch(
        `https://jsonplaceholder.typicode.com/photos?${
          albumId !== "all" ? `albumId=${albumId}` : ``
        }`
      );
      const data = await fetch(
        `https://jsonplaceholder.typicode.com/photos?${
          albumId !== "all" ? `albumId=${albumId}&` : ``
        }_page=${page + 1}&_limit=${limit}`
      );
      const result = await data.json();
      const resultLength = await dataLength.json();
      setPhotos({
        loading: false,
        data: result,
      });
      changeLength(resultLength.length);
    } catch (e) {
      console.error(e);
    }
  }

  const handleDialogChange = (params) => {
    setDialog(params);
  };

  return (
    <>
      {photos.loading ? (
        <LinearColor />
      ) : (
        <Table aria-label="simple table">
          <TableHead
            sx={(theme) => ({
              background: theme.palette.common.black,
              "& > tr > th": {
                color: theme.palette.common.white,
                fontFamily: "monospace",
                fontWeight: "600",
              },
            })}
          >
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>Title</TableCell>
              <TableCell>Image</TableCell>
              <TableCell align="right">Delete</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {photos.data.map((row) => (
              <TableRow
                key={row.id}
                sx={(theme) => ({
                  "&:nth-child(odd)": {
                    backgroundColor: theme.palette.action.hover,
                  },
                })}
              >
                <TableCell component="th" scope="row">
                  {row.id}
                </TableCell>
                <TableCell component="th" scope="row">
                  {row.title}
                </TableCell>
                <TableCell>
                  <img
                    src={row.thumbnailUrl}
                    alt="test"
                    height="50px"
                    width="50px"
                    onClick={() => setModal({ open: true, url: row.url })}
                  />
                </TableCell>
                <TableCell align="right">
                  <Chip
                    label="Delete"
                    color="error"
                    variant="outlined"
                    onClick={() =>
                      setDialog({ open: true, agreement: false, id: row.id })
                    }
                  />
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      )}
      <BasicModal
        open={modal.open}
        close={() => setModal({ open: false, url: null })}
        url={modal.url}
      />
      <ConfirmDialog
        open={dialog.open}
        handleDialogChange={handleDialogChange}
        photoId={dialog.id ?? undefined}
        handleDeletePhoto={deletePhoto}
      />
    </>
  );
}
