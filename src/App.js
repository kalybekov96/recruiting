import { useEffect, useState } from "react";
import {
  Paper,
  TablePagination,
  InputLabel,
  FormControl,
  MenuItem,
  Select,
} from "@mui/material";
import { AppTable } from "./table/table";

function App() {
  const [length, setLength] = useState(0);
  const [albums, setAlbums] = useState([]);
  const [albumId, setAlbumId] = useState("all");
  const [page, setPage] = useState(0);
  const [limit, setLimit] = useState(5);

  useEffect(() => {
    fetchAlbums();
  }, []);

  const handleChangeLength = (length) => {
    setLength(length);
  };

  async function fetchAlbums() {
    try {
      const data = await fetch(`https://jsonplaceholder.typicode.com/albums`);
      const result = await data.json();
      setAlbums(result);
    } catch (e) {
      console.error(e);
    }
  }

  const setRowsPerPage = (event) => {
    setLimit(event.target.value);
    setPage(0);
  };

  return (
    <div>
      <FormControl sx={{ m: "20px", minWidth: "30%" }}>
        <InputLabel id="album-select-label">Choose album</InputLabel>
        <Select
          labelId="album-select-label"
          id="album-select"
          value={albumId}
          onChange={(event) => setAlbumId(event.target.value)}
          label="Choose album"
        >
          <MenuItem value={"all"}>All albums</MenuItem>
          {albums.map((item) => (
            <MenuItem key={item.id} value={item.id}>
              {item.title}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
      <Paper sx={{ m: "20px", overflow: "hidden" }}>
        <AppTable
          albumId={albumId}
          page={page}
          limit={limit}
          changeLength={(number) => handleChangeLength(number)}
        />
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={length}
          rowsPerPage={limit}
          page={page}
          onPageChange={(_, newPage) => setPage(newPage)}
          onRowsPerPageChange={setRowsPerPage}
        />
      </Paper>
    </div>
  );
}

export default App;
