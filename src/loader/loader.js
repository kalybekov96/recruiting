import { Stack, LinearProgress } from "@mui/material";

export function LinearColor() {
  return (
    <Stack
      sx={{ width: "100%", color: "grey.500", padding: '30px 0'}}
      spacing={4}
    >
      <LinearProgress color="secondary" />
      <LinearProgress color="success" />
      <LinearProgress color="inherit" />
    </Stack>
  );
}
